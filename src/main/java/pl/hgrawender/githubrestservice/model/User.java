package pl.hgrawender.githubrestservice.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonSetter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.util.Date;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({ "id", "login", "name", "type", "avatarUrl", "createdAt", "calculations" })     //if we care about order of fields
public class User {

    public User() {
    }

    public User(String login, int requestCount) {
        this.login = login;
        this.requestCount = requestCount;
    }

    @Transient
    private Long id;

    @Id
    @Column(name = "LOGIN", nullable = false, unique = true)
    private String login;

    @Column(name = "REQUEST_COUNT", nullable = false)
    @JsonIgnore
    private int requestCount;

    @Transient
    private String name;
    @Transient
    private String type;
    @Transient
    private String avatarUrl;
    @Transient
    private Date createdAt;
    @Transient
    private Double calculations;

    @Transient
    private int publicRepos;
    @Transient
    private int followers;


    public int getRequestCount() {
        return requestCount;
    }

    public void setRequestCount(int requestCount) {
        this.requestCount = requestCount;
    }

    @JsonIgnore
    public int getPublicRepos() {
        return publicRepos;
    }

    @JsonSetter("public_repos")
    public void setPublicRepos(int publicRepos) {
        this.publicRepos = publicRepos;
    }

    @JsonIgnore
    public int getFollowers() {
        return followers;
    }

    @JsonSetter
    public void setFollowers(int followers) {
        this.followers = followers;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @JsonGetter
    public Double getCalculations() {
        return calculations;
    }

    public void setCalculations(Double calculations) {
        this.calculations = calculations;
    }


    @JsonGetter("avatarUrl")
    public String getAvatarUrl() {
        return avatarUrl;
    }

    @JsonSetter("avatar_url")
    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    @JsonGetter("createdAt")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss'Z'", timezone="GMT")
    public Date getCreatedAt() {
        return createdAt;
    }

    @JsonSetter("created_at")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss'Z'", timezone="GMT")
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return "User{" +
                "login='" + login + '\'' +
                ", requestCount=" + requestCount +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", avatarUrl='" + avatarUrl + '\'' +
                ", createdAt=" + createdAt +
                ", calculations=" + calculations +
                ", publicRepos=" + publicRepos +
                ", followers=" + followers +
                '}';
    }
}
