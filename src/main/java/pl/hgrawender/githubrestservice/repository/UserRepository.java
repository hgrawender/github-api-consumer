package pl.hgrawender.githubrestservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.hgrawender.githubrestservice.model.User;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByLogin(String Login);
}
