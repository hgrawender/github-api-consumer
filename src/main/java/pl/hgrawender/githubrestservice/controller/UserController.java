package pl.hgrawender.githubrestservice.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpStatusCodeException;
import pl.hgrawender.githubrestservice.GithubRestServiceApplication;
import pl.hgrawender.githubrestservice.model.User;
import pl.hgrawender.githubrestservice.service.UserService;
import pl.hgrawender.githubrestservice.repository.UserRepository;
import org.springframework.web.client.RestTemplate;

import javax.validation.constraints.Pattern;
import java.util.Optional;


@RestController
@Validated
public class UserController {

    @Value("${url.github.api.users}")
    private String GITHUB_API_USERS;

    @Value("${calculations.default}")
    private Double CALCULATIONS_DEFAULT;

    private final String MESSAGE = "{\"message\":\"%s\"}";

    @Autowired
    private UserService userService;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private UserRepository userRepository;

    private static final Logger LOGGER = LoggerFactory.getLogger(GithubRestServiceApplication.class);
    ObjectMapper objectMapper = new ObjectMapper();



    @GetMapping(path = "/users/{login}", produces = "application/json")
    public ResponseEntity<String> getUser(@PathVariable @Pattern(regexp = "[A-Za-z0-9]+") String login) {

        int requestCount;

        //Check if this user has been requested previously, otherwise set counter to 1
        Optional<User> foundUser = userRepository.findByLogin(login);
        if (foundUser.isPresent()) {
            User userInDB = foundUser.get();
            requestCount = userInDB.getRequestCount() + 1;
        } else {
            LOGGER.info("User not found in our db, setting request counter to 1");
            requestCount = 1;
        }
        userRepository.save(new User(login, requestCount));


        //Retrieve object from GitHub API
        final User user;
        try {
            user = restTemplate.getForObject(GITHUB_API_USERS + login, User.class);
        } catch (HttpStatusCodeException e) {
            int httpStatusCode = e.getRawStatusCode();
            String messageContent = HttpStatus.NOT_IMPLEMENTED.getReasonPhrase();

            if(httpStatusCode==404) {
                messageContent = HttpStatus.NOT_FOUND.getReasonPhrase();
            }
            return ResponseEntity.status(httpStatusCode)
                    .body(String.format(MESSAGE, messageContent));
        }



        //Perform calculations, save user and prepare response
        String result;
        try {
            Optional<Double> calculations = userService.performCalculations(user);
            if (calculations.isPresent()) {
                user.setCalculations(calculations.get());
            } else {
                user.setCalculations(CALCULATIONS_DEFAULT);
            }
            result = objectMapper.writeValueAsString(user);
        } catch (JsonProcessingException | IllegalArgumentException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(String.format(MESSAGE, HttpStatus.BAD_REQUEST.getReasonPhrase()));
        }
        return ResponseEntity.ok(result);
    }

}