package pl.hgrawender.githubrestservice.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import pl.hgrawender.githubrestservice.GithubRestServiceApplication;
import pl.hgrawender.githubrestservice.model.User;

import java.util.Optional;

@Component
public class UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(GithubRestServiceApplication.class);

    public Optional<Double> performCalculations(User user) {

        double result = 6.0 / user.getFollowers() * (2 + user.getPublicRepos());

        if (Double.isFinite(result)) {
            return Optional.of(result);
        } else {
            LOGGER.warn("Result of the calculations were not a finite number");
            return Optional.empty();
        }
    }

}
