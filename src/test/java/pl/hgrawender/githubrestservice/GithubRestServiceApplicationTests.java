package pl.hgrawender.githubrestservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.client.RestTemplate;
import pl.hgrawender.githubrestservice.model.User;
import pl.hgrawender.githubrestservice.repository.UserRepository;
import pl.hgrawender.githubrestservice.service.UserService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class GithubRestServiceApplicationTests {

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserService userService;

    @Value("${url.github.api.users}")
    private String GITHUB_API_USERS;

    @Value("${calculations.default}")
    private Double CALCULATIONS_DEFAULT;

    private String USERS_ENDPOINT = "http://localhost:8080/users/";

    @Autowired
    private MockMvc mvc;

    private MockRestServiceServer mockServer;

    ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void requestForExistingUser() throws Exception {
        //given
        String login = "octocat";
        User expectedUser = setUpUser(login);


        Optional<Double> calculations = userService.performCalculations(expectedUser);
        if (calculations.isPresent()) {
            expectedUser.setCalculations(calculations.get());
        } else {
            expectedUser.setCalculations(CALCULATIONS_DEFAULT);
        }

        //converting object to JSON
        String userJson = objectMapper.writeValueAsString(expectedUser);


        //Mocking external API (github api)
        mockServer = MockRestServiceServer.createServer(restTemplate);
        mockServer.expect(ExpectedCount.once(), requestTo(GITHUB_API_USERS + login))
                .andRespond(withSuccess(userJson, MediaType.APPLICATION_JSON));

        //when
        String responseContent = callUsersEndpoint(login);
        User actualUser = objectMapper.readValue(responseContent, User.class);


        //then
        assertThat(expectedUser.equals(actualUser));
    }


    @Test
    public void request_counting() throws Exception {
        //Times user will be requested
        int number_of_requests = 2;

        ///given
        String login = "octocat2";
        User expectedUser = setUpUser(login);

        Optional<Double> calculations = userService.performCalculations(expectedUser);
        if (calculations.isPresent()) {
            expectedUser.setCalculations(calculations.get());
        } else {
            expectedUser.setCalculations(CALCULATIONS_DEFAULT);
        }

        String userJson = objectMapper.writeValueAsString(expectedUser);

        //Mocking external API (github api)
        mockServer = MockRestServiceServer.createServer(restTemplate);
        mockServer.expect(ExpectedCount.times(number_of_requests), requestTo(GITHUB_API_USERS + login))
                .andRespond(withSuccess(userJson, MediaType.APPLICATION_JSON));

        //when
        for (int i = 0; i < number_of_requests; i++) {
            callUsersEndpoint(login);
        }
        Optional<User> userInDB = userRepository.findByLogin(login);

        //then
        if (userInDB.isPresent()) {
            User user = userInDB.get();
            assertEquals(number_of_requests, user.getRequestCount());
        } else {
            Assert.fail("User not found!");
        }

    }


    @Test
    public void performing_Calculations_Dividing_By_Zero() throws Exception {
        User user = setUpUser("test");
        user.setFollowers(0);
        Optional<Double> calculations = userService.performCalculations(user);

        Assert.assertFalse(calculations.isPresent());
    }


    private String callUsersEndpoint(String login) throws Exception {
        return mvc.perform(MockMvcRequestBuilders
                .get(USERS_ENDPOINT + login)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
    }

    private User setUpUser(String login) throws ParseException {
        String date = "2011-01-25T19:44:36Z";

        User user = new User();
        user.setId(111l);
        user.setLogin(login);
        user.setName("octocat_name");
        user.setType("User");
        user.setAvatarUrl("img.com/avatar1");
        user.setCreatedAt(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'").parse(date));
        user.setPublicRepos(10);
        user.setFollowers(50);

        return user;
    }
}
