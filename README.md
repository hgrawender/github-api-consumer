# Read Me First
Simple REST service consuming Github API

### Technical suggestions
Please note, that several ways could be used to build application with specification given below:

* One could create a special DTO object for returning messages such as "not found", instead of using String formatting "{\"message\":\"%s\"}". However, in this case author decided it is unnecessary. 
* Optionally one could add the interface for UserService such as IUserService.
* It is not defined whether the REQUEST_COUNT counter should be incremented only by requesting the user even if for example user does not exist in a github database. Application counts it even in that case.
* Author decided that the best way to implement this application was to use only one model class (User.class). However, one could divide it into DTO (for saving response from Github API) and DAO (saving into DB) and adding object mappers.
* Default value for calculations is 0, since case for followers number equal to 0 was not specified in requirements. 
* One could move Beans to separate class annotated with @Configuration

### How to run it

Please run: "**mvn spring-boot:run**" command inside of the project.

### Technical notes
Java version used: "12.0.2"  
Database H2 in-memory is used for simplicity.  
It can be accessed at **http://localhost:8080/h2-console**  
JDBC URL:&nbsp;&nbsp;&nbsp;&nbsp;jdbc:h2:mem:/test  
User Name:&nbsp;sa  
Password:&nbsp;&nbsp;&nbsp;&nbsp;password  

This database is auto created each time the application is started and all data is lost after shutdown.

GET endpoint at **http://localhost:8080/users/{login}**


### Description of application
This application has only one endpoint, which can be called using GET method:

GET /users/{login}

where login is a github user login.

It calls Github API https://api.github.com/users/{login} endpoint
and outputs information about a given user in a JSON format in form:  
 {   
&nbsp;&nbsp;"id": "...",  
&nbsp;&nbsp;"login": "...",  
&nbsp;&nbsp;"name": "...",  
&nbsp;&nbsp;"type": "...",  
&nbsp;&nbsp;"avatarUrl": "...",  
&nbsp;&nbsp;"createdAt": "...",  
&nbsp;&nbsp;"calculations": "..."  
 }

Field calculations is a result of operation:   
&nbsp;&nbsp;6 / followers * (2 + public_repos)

Moreover, the service counts number of requests for given login and saves it in a database. 
